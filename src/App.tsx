import React from "react";
import "./App.css";
import NavBar from "./components/NavBar";
import { Route, BrowserRouter, Routes } from "react-router-dom";
import Startpage from "./pages/Startpage";
import ServicesPage from "./pages/ServicesPage";
import TimeTrackerPage from "./pages/TimeTrackerPage";
import { type } from "os";

type ContextType = {
  trackedTime: number | undefined;
  setTrackedTime: React.Dispatch<React.SetStateAction<number | undefined>>;
};

// @ts-ignore
export const Context: React.Context<ContextType> = React.createContext();
function App() {
  const [trackedTime, setTrackedTime] = React.useState<number>();

  const context = {
    trackedTime,
    setTrackedTime,
  };

  return (
    <Context.Provider value={context}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Startpage />} />
          <Route path="/services" element={<ServicesPage />} />
          <Route path="/time-tracking" element={<TimeTrackerPage />} />
        </Routes>
      </BrowserRouter>
    </Context.Provider>
  );
}

export default App;
