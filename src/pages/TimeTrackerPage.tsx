import { IoMdTime } from "react-icons/io";
import NavBar from "../components/NavBar";
import NavBarLink from "../components/NavBarLink";
import RoundTracker from "../components/RoundTracker";
import TimeTrackingComponent from "../components/TimeTrackingComponent";
import useWindowDimensions from "../hooks/useDimensions";

export default function TimeTrackerPage() {
  const { height, width } = useWindowDimensions();

  return (
    <div>
      <NavBar />
      <TimeTrackingComponent />
    </div>
  );
}
