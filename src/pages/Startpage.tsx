import { MdNavigateNext } from "react-icons/md";
import IconPopup from "../components/IconPopup";
import NavBar from "../components/NavBar";
import NavBarLink from "../components/NavBarLink";
import useWindowDimensions from "../hooks/useDimensions";
import "./Startpage.css";

export default function Startpage() {
  const { height } = useWindowDimensions();

  return (
    <div className="startpage" style={{ height }}>
      <NavBar />
      <div className="startpage-container">
        <NavBarLink
          Icon={MdNavigateNext}
          label="Check out our services"
          size={300}
          link={"/services"}
        />
        {/* <IconPopup label="Check out our services" /> */}
      </div>
    </div>
  );
}
