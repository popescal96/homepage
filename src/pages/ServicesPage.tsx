import { MdNavigateNext } from "react-icons/md";
import IconPopup from "../components/IconPopup";
import NavBar from "../components/NavBar";
import NavBarLink from "../components/NavBarLink";
import ServiceCard from "../components/ServiceCard";
import useWindowDimensions from "../hooks/useDimensions";
import { Service } from "../types/types";
import "./ServicesPage.css";

export default function Startpage() {
  const { height } = useWindowDimensions();

  const services: Service[] = [
    {
      title: "Basic Package",
      offer: "Website with no external data processing",
      price: "600 €",
    },
    {
      title: "Complete Package",
      offer: "Website with external server",
      price: "1,200 €",
    },
  ];

  return (
    <div className="servicespage" style={{ height }}>
      <NavBar />
      <div className="servicespage-container">
        {services.map((s) => (
          <ServiceCard title={s.title} offer={s.offer} price={s.price} />
        ))}
      </div>
    </div>
  );
}
