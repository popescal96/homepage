export type Service = {
  title: string;
  offer: string;
  price: string;
};
