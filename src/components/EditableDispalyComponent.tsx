import React from "react";
import { MdEdit } from "react-icons/md";
import { Context } from "../App";
import NavBarLink from "./NavBarLink";

type Props = {
  display: string;
  record: boolean;
};

export default function EditableDisplayComponent({ display, record }: Props) {
  const context = React.useContext(Context);

  const [shpowInput, setShowInput] = React.useState(false);
  const [hours, setHours] = React.useState<string>("");
  const [minutes, setMinutes] = React.useState<string>("");

  React.useEffect(() => {
    const hrsMS = Number(hours) * 3600000;
    const minMS = Number(minutes) * 60000;
    const totalMS = hrsMS + minMS;
    context.setTrackedTime(totalMS);
  }, [hours, minutes]);

  React.useEffect(() => {
    if (record) {
      setShowInput(false);
    }
  }, [record]);

  if (shpowInput)
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          borderStyle: "solid",
          borderColor: "lightgray",
          justifyContent: "center",
          alignItems: "center",
          borderWidth: 1,
          width: 180,
          height: 50,
          borderRadius: 15,
        }}
      >
        <CustomInput value={hours} setValue={setHours} unit="hr  :" />
        <CustomInput value={minutes} setValue={setMinutes} unit="min" />
      </div>
    );

  return (
    <NavBarLink
      Icon={MdEdit}
      label={display}
      onClick={() => setShowInput(true)}
    />
  );
}

type CustomInputProps = {
  value: string;
  setValue: React.Dispatch<React.SetStateAction<string>>;
  unit: string;
};

function CustomInput({ value, setValue, unit }: CustomInputProps) {
  return (
    <div>
      <input
        style={{
          fontSize: 25,
          paddingLeft: 15,
          width: 40,
          paddingRight: 5,
        }}
        type="number"
        value={value}
        onChange={(e) => {
          setValue(e.target.value);
        }}
      />
      {unit}
    </div>
  );
}
