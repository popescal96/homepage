import "./NavBar.css";
import { Link } from "react-router-dom";
import NavBarLink from "./NavBarLink";
import React from "react";
import { BiHomeSmile } from "react-icons/bi";
import { IoMdContact, IoMdTime } from "react-icons/io";
import { IconType } from "react-icons/lib/esm/iconBase";
import { BsInfoLg } from "react-icons/bs";

export default function NavBar() {
  type Link = {
    Icon: IconType;
    label: string;
    link?: string | undefined;
  };

  const links: Link[] = [
    {
      Icon: BiHomeSmile,
      label: "Startpage",
    },
    {
      Icon: BsInfoLg,
      label: "About Us",
    },
    {
      Icon: IoMdContact,
      label: "Contact",
    },
    {
      Icon: IoMdTime,
      label: "Time Tracker",
      link: "/time-tracking",
    },
  ];

  return (
    <div className="navbar-container">
      <img
        src={require("../assets/logo.png")}
        style={{
          width: 300,
          height: 300,
          position: "absolute",
          left: 200,
          top: -60,
        }}
      />
      <div className="links-container">
        {links.map((link) => (
          <NavBarLink
            Icon={link.Icon}
            label={link.label}
            link={link.link ?? undefined}
          />
        ))}
      </div>
    </div>
  );
}
