import { MdNavigateNext } from "react-icons/md";
import "./IconPopup.css";
import { Colors } from "../constants";

type Props = {
  label: string;
};

export default function IconPopup({ label }: Props) {
  return (
    <div className="iconpopup-container">
      <div style={{ color: Colors.azure, fontSize: 22 }}>{label}</div>
      <MdNavigateNext color={Colors.azure} size={25} />
    </div>
  );
}
