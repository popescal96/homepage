import React from "react";
import { Colors } from "../constants";
import "./NavBarLink.css";
import { IconType } from "react-icons/lib/esm/iconBase";
import { Link } from "react-router-dom";

type Props = {
  Icon: IconType;
  label: string;
  size?: number;
  link?: string;
  onClick?: () => void;
};

export default function NavBarLink({
  Icon,
  label,
  size,
  link,
  onClick,
}: Props) {
  if (onClick)
    return (
      <div
        className="navbarlink-container"
        style={{ width: size ?? undefined, textDecoration: "none" }}
        onClick={() => {
          if (onClick) onClick();
          else return null;
        }}
      >
        <div style={{ color: Colors.blue, fontSize: 22, opacity: 1 }}>
          {label}
        </div>
        <Icon size={25} color={Colors.blue} />
      </div>
    );
  return (
    <Link
      className="navbarlink-container"
      style={{ width: size ?? undefined, textDecoration: "none" }}
      to={link ?? "/"}
    >
      <div style={{ color: Colors.blue, fontSize: 22, opacity: 1 }}>
        {label}
      </div>
      <Icon size={25} color={Colors.blue} />
    </Link>
  );
}
