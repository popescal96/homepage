import { Service } from "../types/types";
import "./ServiceCard.css";

type Props = Service;

export default function ServiceCard({ title, offer, price }: Props) {
  return (
    <div className="servicecard-container">
      <div className="servicecard-title">{title}</div>
      <div className="servicecard-offer">{offer}</div>
      <div className="servicecard-price">{price}</div>
    </div>
  );
}
