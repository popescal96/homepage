import { Colors } from "../constants";
import useWindowDimensions from "../hooks/useDimensions";
import EditableDisplayComponent from "./EditableDispalyComponent";

type Props = {
  display: string;
  progress: number;
  record: boolean;
};

export default function RoundTracker({ display, progress, record }: Props) {
  const { width, height } = useWindowDimensions();
  return (
    <div
      style={{
        width,
        height: height * 0.68,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
      }}
    >
      <div
        style={{
          //   borderStyle: "solid",
          //   borderWidth: 5,
          width: 400,
          height: 400,
          borderRadius: 200,
          justifyContent: "center",
          alignItems: "center",
          display: "flex",
          background:
            //   `linear-gradient(270deg, red ${progress}%, green ${progress}%), linear-gradient(0deg, yellow ${progress}%, ${Colors.blue} ${progress}%)`,
            `linear-gradient(0deg, ${
              Colors.blue
            } ${progress}%, lightgray ${1}%)`,
        }}
      >
        <div
          style={{
            backgroundColor: "white",
            // borderColor: Colors.blue,
            position: "absolute",
            // borderStyle: "solid",
            // borderWidth: 5,
            width: 385,
            height: 385,
            borderRadius: 200,
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
          }}
        >
          <EditableDisplayComponent display={display} record={record} />
        </div>
      </div>
    </div>
  );
}
