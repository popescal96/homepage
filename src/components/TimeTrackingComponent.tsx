import React from "react";
import { IoMdTime } from "react-icons/io";
import { Context } from "../App";
import useWindowDimensions from "../hooks/useDimensions";
import NavBarLink from "./NavBarLink";
import RoundTracker from "./RoundTracker";

export default function TimeTrackingComponent() {
  const { height, width } = useWindowDimensions();
  const context = React.useContext(Context);

  const [timeElapsedPercentage, setTimeElapsedPercentage] = React.useState(0);
  const [time, setTime] = React.useState(0);
  const [displayTime, setDisplayTime] = React.useState(0);
  const [record, setRecord] = React.useState(false);
  const [tracker, setTracker] = React.useState<NodeJS.Timer | null>(null);
  const [timePercentageBeforeBreak, setTimePercentageBeforeBreak] =
    React.useState(0);

  function recordTime() {
    const now = new Date().getTime();
    setTime(now);
    setRecord((prev) => !prev);
  }

  React.useEffect(() => {
    console.log("Record:", record);
    if (record) {
      const tracking = setInterval(() => {
        const now = new Date().getTime();
        const elapsed = now - time;
        const trackedTime = Number(context.trackedTime);
        const percentage = timePercentageBeforeBreak + elapsed / trackedTime;
        console.log("Updating time...", elapsed, percentage);
        setTimeElapsedPercentage(percentage);
        setDisplayTime(trackedTime - elapsed);
        if (percentage > 1) {
          setRecord(false);
          setTimeElapsedPercentage(1);
          alert("Time succesfully recoreded");
        }
      }, 1000);
      setTracker(tracking);
    }
    if (!record) {
      setTimePercentageBeforeBreak(timeElapsedPercentage);
      console.log("Cleared interval", tracker);
      if (tracker) {
        clearInterval(tracker);
        setTracker(null);
      }
    }
  }, [record]);

  return (
    <div
      style={{
        // backgroundColor: "green",
        width,
        height: height * 0.88,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
      }}
    >
      <RoundTracker
        display={new Date(displayTime).toUTCString().slice(16, 25)}
        progress={timeElapsedPercentage * 100}
        record={record}
      />
      <NavBarLink
        label={record ? "Stop" : "Start"}
        Icon={IoMdTime}
        onClick={recordTime}
      />
    </div>
  );
}
